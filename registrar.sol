pragma solidity 0.5.17;

contract Registrar {
    
    event NameRegistered(address _address, string _name);
    event NameUnregistered(address _address);
    
    mapping (address => string) internal correspondings;
    mapping (uint256 => address) internal addresses;
    uint256 internal length = 1;
    
    /**
     * Name: The name registration
     * The method allows to register a name for an account. One account must be allowed to register one name. 
     * If any name already registered for the account the transaction must be reverted. 
     * The name must not be blank. Different accounts are allowed to use the same name for registration.
     * If the name registered successfully, the following event must be emitted: event NameRegistered(address _address, string _name);
     * Consume: The method must not consume more than 80000 gas (if more than five accounts registered the same name this number could exceed)
     */
    function registerName(string memory _name) public {
        /* There is no name registered for this account */
        if (keccak256(abi.encodePacked(correspondings[msg.sender])) == keccak256(abi.encodePacked(""))) {
            if (bytes(_name).length > 0) {
                addAddress(msg.sender);
                correspondings[msg.sender] = _name;
                emit NameRegistered(msg.sender, _name);
            }
            else {
                revert();
            }
        }
        /* The name is already assigned */
        else {
            revert();
        }
    }
    
    /**
     * Name: The name unregistration
     * The method allows to delete a correspondence between a name and an account - the transaction sender. 
     * If an account has no correspondence registered the transaction must be reverted. If the correspondence deleted successfully,
     * the following event must be emitted: event NameUnregistered(address _address);
     * Consume: The method must not consume more than 24000 gas (if more than five accounts registered for the same name this number could exceed)
     */
    function unregisterName() public {
        /* There is correspondence */
        if (keccak256(abi.encodePacked(correspondings[msg.sender])) != keccak256(abi.encodePacked(""))) {
            delete correspondings[msg.sender];
            emit NameUnregistered(msg.sender);
        }
        /* There is no correspondence */
        else {
            revert("No name found for your account");
        }
    }
    
    /**
     * Name: Get the correspondence to an address
     * The method allows to get the name registered for the address. If no name registered for the address the method must return empty sequence. 
     * If the correspondence for a name was deleted by this address previously, the method must return empty sequence.
     * Consume: View function - mustn't consume gas
     */
    function getName(address _address) public view returns (string memory) {
        /* There is correspondence */
        if (keccak256(abi.encodePacked(correspondings[_address])) != keccak256(abi.encodePacked(""))) {
            return correspondings[_address];
        }
        /* There is no correspondence */
        else {
            return "";
        }
    }
    
    /**
     * Name: Get addresses registered for a name
     * The method allows to get all addresses that registered the particular name. If no name registered for the address the method must return empty array.
     * If only one address registered this name, the array must contain only one address. If the correspondence for the name was deleted by an address,
     * this address should not be returned in the array.
     * Consume: View function - mustn't consume gas
     */
    function getAddresses(string memory _name) public view returns (address [] memory) {
        
        if (bytes(_name).length > 0) {
            uint counter = 0;
            
            for (uint256 i=1; i<length; i++){
                if (keccak256(abi.encodePacked(correspondings[addresses[i]])) == keccak256(abi.encodePacked(_name))) {
                    counter++;
                }
            }
            
            address[] memory inter_result = new address[](counter);
            
            counter = 0;
            
            for (uint256 i=1; i<length; i++){
                if (keccak256(abi.encodePacked(correspondings[addresses[i]])) == keccak256(abi.encodePacked(_name))) {
                    
                    bool Exists = false;
                    
                    // Iterate through the result array for duplicates
                    for (uint256 k=0; k<counter; k++) {
                        if (inter_result[k] == addresses[i]){
                            // This address is already in array
                            Exists = true;
                        }
                    }
                    
                    // If this is new address we add it to array
                    if (!Exists) {
                        inter_result[counter] = addresses[i];
                        counter++;
                    }
                }
                
            }
            
            counter = 0;
            
            for (uint256 k=0; k<inter_result.length; k++) {
                if (inter_result[k] != 0x0000000000000000000000000000000000000000){
                    counter++;
                }
            }
            
            address[] memory result = new address[](counter);
            counter = 0;
            
            for (uint256 k=0; k<inter_result.length; k++) {
                if (inter_result[k] != 0x0000000000000000000000000000000000000000){
                    result[counter] = inter_result[k];
                    counter++;
                }
            }
            
            return result;
        }
        else {
            return new address[](0);
        }
    }
    
    /**
     * Name: Getting all registered correspondences
     * The method allows to get all addresses that registered in contract
     * Consume: View function - mustn't consume gas 
     */
    function getAllAddresses() public view returns(address[] memory) {
        
            uint256 sum = 0;
        
            for (uint256 i=1; i<length; i++) {
                if (keccak256(abi.encodePacked(correspondings[addresses[i]])) != keccak256(abi.encodePacked(""))) {
                    sum++;
                }
            }
        
            address [] memory result = new address[](sum);
            uint256 counter = 0;
            
            for (uint256 i=1; i<length; i++) {
                if (keccak256(abi.encodePacked(correspondings[addresses[i]])) != keccak256(abi.encodePacked(""))) {
                    result[counter] = addresses[i];
                    counter++;
                }
            }
            
            return result;
    }
    
    /**
     * Name: Add address to special mapping
     * Add to last position fo the mapping new address and increment the length
     * Consume: Changes the value of the uint256 variable and assign new value for mapping - will be the part of price of the registerName function
     */
    function addAddress(address _address) internal {
                addresses[length] = _address;
                length ++;
    }

    /**
     * Name: Find index of address in special mapping
     * Search for address by iterationg through all the numbers from 1 to the length and return the index of the address in mapping
     * Consume: View fucntion - no gas consuming
     */
    function getAddressIndex(address _address) internal view returns (uint256) {
        for (uint256 i=1; i<length; i++){
            if (addresses[i] == _address)
                return i;
        }
    }
}